﻿using SuperCarte.Core.Models;
using SuperCarte.EF.Data;

namespace SuperCarte.Core.Extensions;

/// <summary>
/// Classe statique qui regroupe les méthodes d'extension pour la conversion (mapping) du modèle Categorie
/// </summary>
public static class CategorieMapExtension
{
    /// <summary>
    /// Convertir un objet Categorie vers un objet CategotieModel
    /// </summary>
    /// <param name="item">Objet à convertir</param>
    /// <returns>Objet converti</returns>
    public static CategorieModel VersCategorieModel(this Categorie item)
    {
        return new CategorieModel()
        {
            CategorieId = item.CategorieId,
            Nom = item.Nom,
            Description = item.Description
        };
    }

    /// <summary>
    /// Convertir une liste d'objet Categorie vers une liste d'objet CategorieModel
    /// </summary>
    /// <param name="lstItem">Liste d'objet à convertir</param>
    /// <returns>Liste d'objet converti</returns>
    public static List<CategorieModel> VersCategorieModel(this List<Categorie> lstItem)
    {
        return lstItem.Select(i => i.VersCategorieModel()).ToList();
    }

    /// <summary>
    /// Convertir un objet CategorieModel vers un objet Categorie
    /// </summary>
    /// <param name="item">Objet à convertir</param>
    /// <returns>Objet converti</returns>
    public static Categorie VersCategorie(this CategorieModel item) 
    {
        return new Categorie()
        {
            CategorieId = item.CategorieId,
            Nom = item.Nom,
            Description = item.Description
        };
    }

    /// <summary>
    /// Convertir une liste d'objet CategorieModel vers une liste d'objet Categorie
    /// </summary>
    /// <param name="lstItem">Liste d'objet à convertir</param>
    /// <returns>Liste d'objet converti</returns>
    public static List<Categorie> VersCategorieModel(this List<CategorieModel> lstItem)
    {
        return lstItem.Select(i => i.VersCategorie()).ToList();
    }

    /// <summary>
    /// Méthode qui copie les valeurs des propriétés de l'objet de donnée Categorie dans l'objet du modèle CategorieModel
    /// </summary>
    /// <param name="itemDestination">CategorieModel à recevoir la copie (destination)</param>
    /// <param name="categorieSource">L'objet Categoriede référence pour la copie (source)</param>
    /// <param name="copierClePrimaire">Copier de la clé primaire</param>
    public static void Copie(this CategorieModel itemDestination, Categorie categorieSource, bool copierClePrimaire)
    {
        if (copierClePrimaire == true)
        {
            itemDestination.CategorieId = categorieSource.CategorieId;
        }

        itemDestination.Nom = categorieSource.Nom;
        itemDestination.Description = categorieSource.Description;
    }

    /// <summary>
    /// Méthode qui copie les valeurs des propriétés du CatégorieModel dans l'objet de donnée Categorie
    /// </summary>
    /// <param name="itemDestination">Categorie à recevoir la copie (destination)</param>
    /// <param name="categorieModelSource">L'objet CategorieModel de référence pour la copie (source)</param>
    /// <param name="ignoreClePrimaire">Ignore la copie de la clé primaire</param>
    public static void Copie(this Categorie itemDestination, CategorieModel categorieModelSource, bool ignoreClePrimaire = true)
    {
        if(ignoreClePrimaire == false)
        {
            itemDestination.CategorieId = categorieModelSource.CategorieId;
        }

        itemDestination.Nom = categorieModelSource.Nom;
        itemDestination.Description = categorieModelSource.Description;
    }
}
