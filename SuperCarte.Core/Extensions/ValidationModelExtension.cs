﻿using FluentValidation.Results;
using SuperCarte.Core.Models;

namespace SuperCarte.Core.Extensions;

/// <summary>
/// Classe statique qui regroupe les méthodes d'extension pour la conversion (mapping) du modèle ValidationModel
/// </summary>
public static class ValidationModelExtension
{
    /// <summary>
    /// Convertir un objet ValidationResult vers un objet ValidationModel
    /// </summary>
    /// <param name="validationResult">Objet à convertir</param>
    /// <returns>Objet converti</returns>
    public static ValidationModel VersValidationModel(this ValidationResult validationResult)
    {
        ValidationModel validationModel = new ValidationModel();

        if (validationResult.IsValid == false)
        {
            foreach(var erreur in validationResult.Errors)
            {
                validationModel.AssignerErreur(erreur.PropertyName, erreur.ErrorMessage);
            }
        }
        return validationModel;
    }
}
