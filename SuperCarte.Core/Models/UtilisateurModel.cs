﻿namespace SuperCarte.Core.Models;

/// <summary>
/// Classe qui contient l'information d'un utilisateur
/// </summary>
public class UtilisateurModel
{
    public int UtilisateurId { get; set; }

    public string Prenom { get; set; } = null!;

    public string Nom { get; set; } = null!;

    public string NomUtilisateur { get; set; } = null!;    

    public int RoleId { get; set; }
}
