﻿using SuperCarte.Core.Repositories.Bases;
using SuperCarte.EF.Data;

namespace SuperCarte.Core.Repositories;

/// <summary>
/// Interface qui contient les méthodes de communication avec la base de données pour la table Ensemble
/// </summary>
public interface IEnsembleRepo : IBasePKUniqueRepo<Ensemble, int>
{
}
