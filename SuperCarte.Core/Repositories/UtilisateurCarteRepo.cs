﻿using Microsoft.EntityFrameworkCore;
using SuperCarte.Core.Models;
using SuperCarte.Core.Repositories.Bases;
using SuperCarte.EF.Data;
using SuperCarte.EF.Data.Context;

namespace SuperCarte.Core.Repositories;

/// <summary>
/// Classe qui contient les méthodes de communication avec la base de données pour la table UtilisateurCarte
/// </summary>
public class UtilisateurCarteRepo : BaseRepo<UtilisateurCarte>, IUtilisateurCarteRepo
{
    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="bd">Contexte de la base de données</param>
    public UtilisateurCarteRepo(SuperCarteContext bd) : base(bd)
    {
        //Vide, il sert uniquement a recevoir le contexte et à l'envoyer à la classe parent.
    }

    public async Task<List<QuantiteCarteDetailModel>> ObtenirCartesUtilisateurAsync(int utilisateurId)
    {
        return await (from lqUtilisateurCarte in _bd.UtilisateurCarteTb
                      where
                          lqUtilisateurCarte.UtilisateurId == utilisateurId
                      orderby
                          lqUtilisateurCarte.Carte.Categorie.Nom,                          
                          lqUtilisateurCarte.Carte.Nom
                      select
                          new QuantiteCarteDetailModel()
                          {
                              CarteId = lqUtilisateurCarte.Carte.CarteId,
                              Nom = lqUtilisateurCarte.Carte.Nom,
                              Vie = lqUtilisateurCarte.Carte.Vie,
                              Armure = lqUtilisateurCarte.Carte.Armure,
                              Attaque = lqUtilisateurCarte.Carte.Attaque,
                              EstRare = lqUtilisateurCarte.Carte.EstRare,
                              PrixRevente = lqUtilisateurCarte.Carte.PrixRevente,
                              CategorieId = lqUtilisateurCarte.Carte.CategorieId,
                              CategorieNom = lqUtilisateurCarte.Carte.Categorie.Nom,
                              EnsembleId = lqUtilisateurCarte.Carte.EnsembleId,
                              EnsembleNom = lqUtilisateurCarte.Carte.Ensemble.Nom,
                              Quantite = lqUtilisateurCarte.Quantite
                          }).ToListAsync();
    }
}
