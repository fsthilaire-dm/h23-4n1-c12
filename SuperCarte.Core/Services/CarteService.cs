﻿using SuperCarte.Core.Models;
using SuperCarte.Core.Repositories;

namespace SuperCarte.Core.Services;

/// <summary>
/// Classe qui contient les services du modèle Carte
/// </summary>
public class CarteService : ICarteService
{
    private readonly ICarteRepo _carteRepo;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="categorieRepo">Repository Carte</param>
    public CarteService(ICarteRepo carteRepo)
    {
        _carteRepo = carteRepo;
    }

    public async Task<List<CarteDetailModel>> ObtenirListeCarteDetailAsync()
    {
        return await _carteRepo.ObtenirListeCarteDetailAsync();
    }
}
