﻿using SuperCarte.Core.Models;

namespace SuperCarte.Core.Services;

/// <summary>
/// Interface qui contient les services du modèle Categorie
/// </summary>
public interface ICategorieService
{
    /// <summary>
    /// Obtenir la liste de catégories en asynchrone.
    /// </summary>
    /// <returns>Liste de catégories</returns>
    Task<List<CategorieModel>> ObtenirListeAsync();    

    /// <summary>
    /// Obtenir les dépendances d'une catégorie.
    /// </summary>
    /// <param name="categorieId">Clé primaire de la catégorie</param>
    /// <returns>Les dépendances ou null si la catégorie n'est pas trouvée</returns>
    CategorieDependance? ObtenirDependance(int categorieId);

    /// <summary>
    /// Supprimer une catégorie en asynchrone.
    /// </summary>    
    /// <param name="categorieId">Clé primaire de la catégorie</param>    
    Task SupprimerAsync(int categorieId);

    /// <summary>
    /// Ajouter une catégorie en asynchrone.
    /// </summary>
    /// <param name="categorieModel">Catégorie à ajouter</param>     
    /// <returns>Vrai si ajoutée, faux si non ajoutée</returns>
    Task<bool> AjouterAsync(CategorieModel categorieModel);

    /// <summary>
    /// Modifier une catégorie en asynchrone.
    /// </summary>
    /// <param name="categorieModel">Catégorie à modifier</param>        
    /// <returns>Vrai si ajoutée, faux si non ajoutée</returns>
    Task<bool> ModifierAsync(CategorieModel categorieModel);

    /// <summary>
    /// Obtenir une catégorie en partir de sa clé primaire en asynchrone.
    /// </summary>
    /// <param name="categorieId">Clé primaire de la catégorie</param>
    /// <returns>La catégorie ou null si la catégorie n'est pas trouvée</returns>
    Task<CategorieModel?> ObtenirAsync(int categorieId);

    /// <summary>
    /// Obtenir une catégorie en partir de sa clé primaire.
    /// </summary>
    /// <param name="categorieId">Clé primaire de la catégorie</param>
    /// <returns>La catégorie ou null si la catégorie n'est pas trouvée</returns>
    CategorieModel? Obtenir(int categorieId);

    /// <summary>
    /// Valider le modèle
    /// </summary>
    /// <param name="categorieModel">CategorieModel à valider</param>
    /// <returns>Résultat de validation</returns>
    Task<ValidationModel> ValiderAsync(CategorieModel categorieModel);
}