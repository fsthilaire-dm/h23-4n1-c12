﻿namespace SuperCarte.EF.Data;

public class UtilisateurCarte
{
    public int UtilisateurId { get; set; }

    public int CarteId { get; set; }

    public short Quantite { get; set; }

    public Utilisateur Utilisateur { get; set; } = null!;

    public Carte Carte { get; set; } = null!;
}