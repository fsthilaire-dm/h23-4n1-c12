﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace SuperCarte.EF.Migrations
{
    /// <inheritdoc />
    public partial class Seed_RoleEtUtilisateur : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "RoleId", "Nom" },
                values: new object[,]
                {
                    { 1, "Administrateur" },
                    { 2, "Utilisateur" }
                });

            migrationBuilder.InsertData(
                table: "Utilisateur",
                columns: new[] { "UtilisateurId", "MotPasseHash", "Nom", "NomUtilisateur", "Prenom", "RoleId" },
                values: new object[,]
                {
                    { 1, "$2y$11$IY6NG9FkTSI1dnjLfSbuOuNkuyI7IZHxHSOD5Td6AlwvroUz/vzLK", "St-Hilaire", "fsthilaire", "François", 1 },
                    { 2, "$2y$11$ewK3YsMGQ1IMKEzJUAjyVe0P19I0gEbTO998mwfVbSSA8nZ6MG/ha", "Tremblay", "btremblay", "Benoit", 2 },
                    { 3, "$2y$11$VfcNowkWResPQKl0AA3MJ.w1LXBqmMM77YKlyf32Glr9TWG4xxyD2", "Stark", "tstark", "Tony", 2 }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Utilisateur",
                keyColumn: "UtilisateurId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Utilisateur",
                keyColumn: "UtilisateurId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Utilisateur",
                keyColumn: "UtilisateurId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "RoleId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "RoleId",
                keyValue: 2);
        }
    }
}
