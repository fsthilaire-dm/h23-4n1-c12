﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace SuperCarte.EF.Migrations
{
    /// <inheritdoc />
    public partial class Seed_CartesEtUtilisateurCartes : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Carte",
                columns: new[] { "CarteId", "Armure", "Attaque", "CategorieId", "EnsembleId", "EstRare", "Nom", "PrixRevente", "Vie" },
                values: new object[,]
                {
                    { 4, (short)11, (short)35, 2, 1, true, "Rider", 3.98m, (short)45 },
                    { 5, (short)0, (short)15, 2, 1, false, "Troll", 0.19m, (short)25 },
                    { 6, (short)10, (short)10, 1, 1, false, "Dragon de glace", 0.09m, (short)35 }
                });

            migrationBuilder.InsertData(
                table: "UtilisateurCarte",
                columns: new[] { "CarteId", "UtilisateurId", "Quantite" },
                values: new object[,]
                {
                    { 1, 1, (short)2 },
                    { 2, 1, (short)5 },
                    { 3, 1, (short)3 },
                    { 1, 3, (short)5 },
                    { 3, 3, (short)1 },
                    { 4, 1, (short)1 },
                    { 6, 3, (short)2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_UtilisateurCarte_CarteId",
                table: "UtilisateurCarte",
                column: "CarteId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_UtilisateurCarte_CarteId",
                table: "UtilisateurCarte");

            migrationBuilder.DeleteData(
                table: "Carte",
                keyColumn: "CarteId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "UtilisateurCarte",
                keyColumns: new[] { "CarteId", "UtilisateurId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "UtilisateurCarte",
                keyColumns: new[] { "CarteId", "UtilisateurId" },
                keyValues: new object[] { 2, 1 });

            migrationBuilder.DeleteData(
                table: "UtilisateurCarte",
                keyColumns: new[] { "CarteId", "UtilisateurId" },
                keyValues: new object[] { 3, 1 });

            migrationBuilder.DeleteData(
                table: "UtilisateurCarte",
                keyColumns: new[] { "CarteId", "UtilisateurId" },
                keyValues: new object[] { 4, 1 });

            migrationBuilder.DeleteData(
                table: "UtilisateurCarte",
                keyColumns: new[] { "CarteId", "UtilisateurId" },
                keyValues: new object[] { 1, 3 });

            migrationBuilder.DeleteData(
                table: "UtilisateurCarte",
                keyColumns: new[] { "CarteId", "UtilisateurId" },
                keyValues: new object[] { 3, 3 });

            migrationBuilder.DeleteData(
                table: "UtilisateurCarte",
                keyColumns: new[] { "CarteId", "UtilisateurId" },
                keyValues: new object[] { 6, 3 });

            migrationBuilder.DeleteData(
                table: "Carte",
                keyColumn: "CarteId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Carte",
                keyColumn: "CarteId",
                keyValue: 6);
        }
    }
}
