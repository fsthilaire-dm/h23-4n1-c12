﻿namespace SuperCarte.WPF.Aides;

/// <summary>
/// Inteface qui représente la classe d'assistance pour l'authentification et l'autorisation
/// </summary>
public interface IAuthentificateur
{
    /// <summary>
    /// Authentifie un utilisateur à partir de son nom d'utilisateur et de son mot de passe en asynchrone.
    /// </summary>
    /// <param name="nomUtilisateur">Nom d'utilisateur</param>
    /// <param name="motPasse">Mot de passe en clair</param>
    /// <returns>Vrai si un utilisateur est authentifié, faux sinon</returns>
    Task<bool> AuthentifierUtilisateurAsync(string nomUtilisateur, string motPasse);
       
    /// <summary>
    /// Vérifie si l'utilisateur est autorisé en fonction des rôles spécifiés en asynchrone.
    /// </summary>
    /// <param name="nomRoles">Nom des rôles autorisés</param>
    /// <returns>Vrai si autorisé, faux si non autorisé</returns>
    Task<bool> EstAutoriseAsync(params string[] nomRoles);

    /// <summary>
    /// Vérifie si l'utilisateur est autorisé en fonction des rôles spécifiés.
    /// </summary>
    /// <param name="nomRoles">Nom des rôles autorisés</param>
    /// <returns>Vrai si autorisé, faux si non autorisé</returns>
    bool EstAutorise(params string[] nomRoles);

    UtilisateurAuthentifieModel? UtilisateurAuthentifie { get; }

    bool EstAuthentifie { get; }
}
