﻿namespace SuperCarte.WPF.Aides;

/// <summary>
/// Inteface qui représente la classe d'assistance pour les notifications avec l'utilisateur
/// </summary>
public interface INotification
{
    /// <summary>
    /// Affiche un message d'erreur à l'utilisateur
    /// </summary>
    /// <param name="titre">Titre du message</param>
    /// <param name="message">Le message d'erreur</param>
    void MessageErreur(string titre, string message);
}
