﻿using System.Windows;

namespace SuperCarte.WPF.Aides;

/// <summary>
/// Classe qui représente la classe d'assistance pour les notifications avec l'utilisateur
/// </summary>
public class Notification : INotification
{
    public void MessageErreur(string titre, string message)
    {
        MessageBox.Show(message, titre, MessageBoxButton.OK, MessageBoxImage.Error);
    }    
}
