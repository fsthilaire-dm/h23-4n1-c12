﻿using CommunityToolkit.Mvvm.ComponentModel;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using SuperCarte.Core.Models;
using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;

namespace SuperCarte.WPF.ViewModels.Bases;

/// <summary>
/// Classe abstraite pour du View Models
/// </summary>
public abstract class BaseVM : ObservableObject, INotifyDataErrorInfo
{
    private readonly Dictionary<string, List<string>> _lstErreursParPropriete = new Dictionary<string, List<string>>();
   
    public event EventHandler<DataErrorsChangedEventArgs>? ErrorsChanged;

    private void OnErrorsChanged(string propertyName)
    {
        ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
    }

    public IEnumerable GetErrors(string? propertyName)
    {
        return _lstErreursParPropriete.GetValueOrDefault(propertyName, null);
    }

    /// <summary>
    /// Effacer les erreurs de la vue
    /// </summary>
    protected void EffacerErreurs()
    {
        foreach (string propriete in _lstErreursParPropriete.Keys)
        {
            _lstErreursParPropriete.Remove(propriete);
            OnErrorsChanged(propriete);
        }
    }

    /// <summary>
    /// Assigner les erreurs à la vue à partir de la validation
    /// </summary>
    /// <param name="validationModel">Objet de validation</param>
    protected void AssignerValidation(ValidationModel validationModel)
    {
        EffacerErreurs();

        foreach (string propriete in validationModel.ErreurParPropriete.Keys)
        {
            if (!_lstErreursParPropriete.ContainsKey(propriete))
            {
                _lstErreursParPropriete.Add(propriete, new List<string>());
            }

            _lstErreursParPropriete[propriete].Add(validationModel.ErreurParPropriete[propriete]);
            OnErrorsChanged(propriete);
        }
    }

    /// <summary>
    /// Ajouter une erreur pour une propriété
    /// </summary>
    /// <param name="propriete">Nom de la propriété</param>
    /// <param name="erreur">Message d'erreur</param>
    protected void AjouterErreur(string propriete, string erreur)
    {
        if (!_lstErreursParPropriete.ContainsKey(propriete))
        {
            _lstErreursParPropriete.Add(propriete, new List<string>());
        }

        _lstErreursParPropriete[propriete].Add(erreur);

        OnErrorsChanged(propriete);
    }

    public bool HasErrors
    {
        get
        {
            return _lstErreursParPropriete.Any();
        }
    }
}
