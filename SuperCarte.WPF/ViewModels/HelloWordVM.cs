﻿using CommunityToolkit.Mvvm.Input;
using System.ComponentModel;

namespace SuperCarte.WPF.ViewModels
{
    public class HelloWordVM : BaseVM
    {
        private DateTime _dateTime;
        private bool _estEnTravail;

        public HelloWordVM()
        {
            RafraichirDateHeureCommande = new AsyncRelayCommand(RafraichirDateHeureAsync);            
        }

        /// <summary>
        /// Rafraichir la date et l'heure
        /// </summary>
        private async Task RafraichirDateHeureAsync()
        {
            EstEnTravail = true;
            await Task.Delay(5000);
            DateHeure = DateTime.Now;
            EstEnTravail = false;
        }

        public IAsyncRelayCommand RafraichirDateHeureCommande { get; private set; }

        public DateTime DateHeure 
        {
            get
            {
                return _dateTime;
            }
            set
            {
                SetProperty(ref _dateTime, value);
            }
        }

        public bool EstEnTravail 
        {
            get
            {
                return _estEnTravail;
            }
            set
            {
                SetProperty(ref _estEnTravail, value);
            }
        }
    }
}